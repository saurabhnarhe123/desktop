//============================================================================
// Name        : ExpTree.cpp
// Author      : Saurabh Narhe
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Construct an expression tree from postfix expression and perform recursive and
//						 non-recursive inorder, preorder and postorder treversals
//============================================================================

#include <iostream>
using namespace std;
#define MAX 20

typedef struct TreeNode {
	char data;
	TreeNode *leftChild,  *rightChild;
}TreeNode;

typedef struct StackNode {
	TreeNode *data;
	StackNode *next;
}StackNode;

class Tree {
public :
	TreeNode *head;
	StackNode *stackTop;

	Tree() {
		head = NULL;
		stackTop = NULL;
	}

	// Stack Operations
	void push(TreeNode *);
	TreeNode* pop();
	int isEmpty();
	int isFull();

	// Expression Tree Creation
	int isOp(char);
	void createTree(char[]);

	// Traversal with recursion
	void inOrder(TreeNode *);
	void preOrder(TreeNode *);
	void postOrder(TreeNode *);

	// Traversal without recursion
	void inOredeWO();
	void preOredeWO();
	void postOredeWO();

};

void Tree::push(TreeNode *data) {
	 if(stackTop == NULL) {
		 stackTop = new StackNode;
		 stackTop -> data = data;
	} else {
		StackNode *temp = new StackNode;
		temp -> data = data;
		temp -> next = stackTop;
		stackTop = temp;
	}
}

TreeNode* Tree::pop() {
	TreeNode *data;
	if(stackTop == NULL) {
		cout<<"\nCant delete stack underflow...";
	} else {
		data = stackTop -> data;
		stackTop = stackTop -> next;
	}
	return data;
}

int Tree::isOp(char ch) {
	if(ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch== '$')
		return 1;
	else
		return 0;
}

void Tree::createTree(char post[]) {
	int i=0;

	while(post[i]!='\0') {

		if(isOp(post[i])){
			if(head == NULL) {
				head = new TreeNode;
				head -> data = post[i];
				if(stackTop != NULL) {
					head -> rightChild =  pop();
				}
				if(stackTop != NULL) {
					head -> leftChild = pop();
				}
				push(head);

			} else {
				TreeNode *temp = new TreeNode;
				temp -> data = post[i];
				if(stackTop != NULL) {
					temp -> rightChild =  pop();
				}
				if(stackTop != NULL) {
					temp -> leftChild = pop();
				}
				head = temp;
				push(temp);
			}
		} else {
			TreeNode *data = new TreeNode;
			data -> leftChild = NULL;
			data -> rightChild = NULL;
			data -> data = post[i];
			push(data);
		}
		i++;
	}
}

void Tree::inOrder(TreeNode *temp) {
	if(temp!=NULL) {
		inOrder(temp->leftChild);
		cout<<"\t"<<temp->data;
		inOrder(temp->rightChild);
	}
}

void Tree::preOrder(TreeNode *temp) {
	if(temp!=NULL) {
		cout<<"\t"<<temp->data;
		preOrder(temp->leftChild);
		preOrder(temp->rightChild);
	}
}

void Tree::postOrder(TreeNode *temp) {
	if(temp!=NULL) {
		postOrder(temp->leftChild);
		postOrder(temp->rightChild);
		cout<<"\t"<<temp->data;
	}
}

void Tree::inOredeWO() {
	stackTop = NULL;
	TreeNode *p = head;
	do {
		while(p!=NULL) {
			push(p);
			p = p -> leftChild;
		}
		p = pop();
		cout<<"\t"<<p->data;
		p = p -> rightChild;
	} while(p!=NULL || stackTop != NULL);

}

void Tree::preOredeWO() {
	stackTop = NULL;
	TreeNode *p = head;
	do {
		while(p!=NULL) {
			cout<<"\t"<<p->data;
			push(p);
			p = p -> leftChild;
		}
		p = pop();
		p = p -> rightChild;
	} while(p!=NULL || stackTop != NULL);
}

void Tree::postOredeWO() {

	stackTop = NULL;
	TreeNode *p = head;
	do {
		while(p!=NULL) {
			push(p);
		}
	} while(p!=NULL || stackTop != NULL);

}

int main() {

	int ch;
	char post[20];
	Tree T;

	cout<<"\nEnter postfix expression:";
	cin>>post;
	T.createTree(post);

	do {
		cout<<"\n1.Inorder \n2.Preorder \n3.Postorder \n4.Inorder Non Recursive \n5.Preorder Non Recursive \n6.Postorder Non Recursive \n7.Exit\n";
		cin>>ch;
		switch(ch) {

			case  1:
				T.inOrder(T.head);
				break;
			case  2:
				T.preOrder(T.head);
				break;
			case  3:
				T.postOrder(T.head);
				break;
			case 4:
				T.inOredeWO();
				break;
			case 5:
				T.preOredeWO();
				break;
			case 6:
				T.postOredeWO();
				break;
			case 7:
				goto exit;
			default :
				cout<<"\nWrong choice please try again...";
		}

	} while(1);

	exit:
	return 0;
}

