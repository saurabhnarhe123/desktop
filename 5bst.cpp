/*
 * main.cpp
 *
 *  Created on: 03-Apr-2017
 *      Author: avinash
 */

#include<iostream>
#include<string.h>
#include<stdlib.h>

using namespace std;

typedef struct node
{
	char data[30];
	struct node *l,*r;
}node;
class tree
{

public:
  node *root;
	tree()
	{
		root=NULL;
	}

void Create_Bst();
void Insert_Node(char[]);
void Display_Bst(node * );
int Height_Bst(node *);
node * Del_Node(node*,char[]);
};

int main()
{
	int s,x;
	tree t;
	char ch,val[30];

	do
	{
	cout <<"\nMENU:\n1-Create\n2-Insert\n3-Display\n4-Height\n5-Delete";
	cout <<"\nEnter your choice:";
	cin>>s;
	switch(s)
	{
		case 1:

			t.Create_Bst();
			break;
		case 2:
				cout <<"Enter the data:";
				cin>>val;

				t.Insert_Node(val);
			break;
		case 3:
				if(t.root==NULL)
					cout <<"\nTree is Empty";
				else

				t.Display_Bst(t.root);
			break;
		case 4:
			int h;
			if(t.root==NULL)
				cout << "Tree Empy";
			else
			{
				h=t.Height_Bst(t.root);
				cout<<"\n Height of BST is: "<< h;
			}
			break;
		case 5:
			if(t.root==NULL)
				cout <<"\nTree is Empty\n";
			else
			{
			cout << "\nEnter Data To Delete: ";
			cin>> val;
			 t.Del_Node(t.root,val);
			}
			break;

	}
	}
	while(1);

}
void tree::Create_Bst()
{
	int n,i;
	char val[30];
		root=NULL;
	cout <<"\nEnter the number of nodes: ";
	cin>>n;
	for(i=0;i<n;i++)
	{
	cout <<"\nEnter the data: ";
		 cin>>val;

	       Insert_Node(val);
	}
}
void tree :: Insert_Node(char val[])
{
	node *temp1,*temp2,*p;
	p=new node;
		 strcpy(p->data,val);
		 p->l=NULL;
		 p->r=NULL;
	if(root==NULL)
	{

		 root=p;
	}
	else
	{
		temp1=root;
		while(temp1!=NULL)
		{
			temp2=temp1;
			if(strcmp(temp1->data,val)>0)
			{
				temp1=temp1->l;
			}
			else
			{
				temp1=temp1->r;
			}
		}

		if(strcmp(temp2->data,val)>0)
		{
		   temp2->l=(node*)malloc(sizeof(node));
		   temp2=temp2->l;
		   strcpy(temp2->data,val);
		   temp2->r=NULL;
		   temp2->l=NULL;
		}
		else
		{
			temp2->r=(node*)malloc(sizeof(node));
			temp2=temp2->r;
			strcpy(temp2->data,val);
			temp2->r=NULL;
			temp2->l=NULL;
		}
	}

}

void tree :: Display_Bst(node *p)
{

	if(p!=NULL)
	{

	Display_Bst(p->l);
	cout <<" "<<p->data;
	Display_Bst(p->r);
	}
}

int tree :: Height_Bst(node *p)
{
int hl,hr;
if(p==NULL)
	//cout<<"TREE EMPTY";
	return 0;

if(p->l==NULL && p->r==NULL)
	return 1;

	hl=Height_Bst(p->l);
	hr=Height_Bst(p->r);

	if (hl>hr)
		return hl+1;
	else
		return hr+1;
}


node * tree :: Del_Node(node * T, char val[])
{
node *temp;
	if(T==NULL)
	{
		cout <<"\nData Not Found";
		return NULL;
	}

	if(strcmp(T->data,val)>0)
	{
		T->l=Del_Node(T->l,val);
		return T;
	}

	if(strcmp(T->data,val)<0)
	{
		T->r=Del_Node(T->r,val);
		return T;
	}

	if(T->l == NULL && T->r==NULL)
	{

		temp=T;
		delete temp;
		return NULL;
	}

	if(T->l==NULL)
	{
		temp=T;
		T=T->r;
		delete temp;
		return T;
	}
	if(T->r==NULL)
	{
		temp=T;
		T=T->l;
		delete temp;
		return T;
	}
	temp=T->r;
	while(temp->l!=NULL)
			temp=temp->l;
	strcpy(T->data,temp->data);
	T->r=Del_Node(T->r,temp->data);
	return T;

}



