/*
 * main.cpp
 *
 *  Created on: 03-Apr-2017
 *      Author: avinash
 */


#include<iostream>
#include<string.h>

using namespace std;

typedef struct node
{
	char data;
	struct node *left;
	struct node *right;
}node;

//==============================

class tree
{
	node *st[40];
	int st1[20];
	int top,top1;

	public:
         tree()
         {
         	top=top1=0;
         }
	void push(node *);
	int empty();
	node *pop();
	void exp();
	void inorder(node *x);
	void preorder(node *x);
	void postorder(node *x);
	void ninorder(node *t);
	void npreorder(node *t);
	void npostorder(node *t);
	void push1(int flag);
	int pop1();
	 int val(char *p);
	};

//============================

void tree :: push(node *x)
{
	top++;
	st[top] = x;
}


node * tree :: pop()
{
	node *x;
	x=st[top];
	top--;
	return x;
}

//========================

int tree :: empty()
{
	if(top==0)
	return 1;
	return 0;
}

//=======================
void tree ::push1(int flag)
{
top1++;
st1[top1]=flag;
}


int tree ::pop1()
{
int flag;
flag=st1[top1];
top1--;
return flag;
}

//=======================

void tree :: exp()
{
	int i=0,f,a,ans,ch;
	top=top1=0;
	char p[20];
	node *t;

	cout<<"\nEnter Postfix Expression : ";
	cin>>p;
	a=val(p);
	if(a==1)
	{
	while(p[i]!='\0')
	{
		if(isalnum(p[i]))
		{
			t= new node;
			t->data=p[i];
			t->left=NULL;
			t->right=NULL;
			push(t);
		}
		else
		{
			t= new node;
			t->data=p[i];
			if(!empty())
			{
				t->right=pop();
			}
		 	else
			{
				f=1;
				break;
			}

			if(!empty())
			{
				t->left=pop();
			}
		 	else
			{
				f=1;
				break;
			}


			push(t);
		}
		i++;
	}

	if(f!=1)
	{
		t=pop();

		do
		{

		cout<<"\nMenu ";
		cout<<"\n1.Inorder Tree (recursive)";
		cout<<"\n2.Preorder Tree (recursive)";
		cout<<"\n3.Postorder Tree (recursive)";
		cout<<"\n4.Inorder Tree (Non-recursive)";
		cout<<"\n5.Preorder Tree (Non-recursive)";
		cout<<"\n6.Postorder Tree (Non-recursive)";
		cout<<"\nEnter your choice :  ";
		cin>>ch;

		switch(ch)
		{
		case 1: cout<<"\nInorder Tree (recursive) Expression Is : ";
			inorder(t);
			cout<<"\n-----------------------------------------------------\n";
			break;

		case 2:	cout<<"\nPreorder Tree (recursive) Expression Is : ";
			preorder(t);
			cout<<"\n------------------------------------------------------\n";
			break;

		case 3:	cout<<"\nPostorder Tree (recursive) Expression Is : ";
			postorder(t);
			cout<<"\n------------------------------------------------------\n";
			break;

		case 4:	cout<<"\nInorder Tree (Non-recursive) Expression Is : ";
			ninorder(t);
			cout<<"\n------------------------------------------------------\n";
			break;

		case 5:	cout<<"\nPreorder Tree (Non-recursive) Expression Is : ";
			npreorder(t);
			cout<<"\n------------------------------------------------------\n";
			break;

		case 6:	cout<<"\nPostorder Tree (Non-recursive) Expression Is : ";
			npostorder(t);
			cout<<"\n------------------------------------------------------\n";
			break;
			}

		cout<<"\nContinue :  ";
		cin>>ans;
		}while(ans==1);

	}

	else
	{
	cout<<"\nInvalid Expression";
	cout<<"\n";
	}
	}
 }

//===========================================================

void tree :: inorder(node *x)
{
	if(x!=NULL)
	{
		inorder(x->left);
		cout<<" "<<x->data;
		inorder(x->right);
	}
}

//=================================================

void tree :: preorder(node *x)
{
	if(x!=NULL)
	{
		cout<<" "<<x->data;
		preorder(x->left);
		preorder(x->right);
	}
}

//============================================

void tree :: postorder(node *x)
{
	if(x!=NULL)
	{
		postorder(x->left);
		postorder(x->right);
		cout<<" "<<x->data;
	}
}

//===========================================

void tree :: ninorder(node *t)
{
	top =0;

	if(t==NULL)
	return ;

	do
	{
		while(t!=NULL)
		{
			push(t);
			t=t->left;
		}

			t=pop();
			cout<<" "<<t->data;
			t=t->right;

	}while(t!=NULL || !empty());


}

//=============================================

void tree :: npreorder(node *t)
{
	top =0;

	if(t==NULL)
	return ;

	do
	{
		while(t!=NULL)
		{
			cout<<" "<<t->data;
			push(t);
			t=t->left;
		}

			t=pop();

			t=t->right;

	}while(t!=NULL || !empty());


}

//===================================================
void tree :: npostorder(node * t)
{
int flag;
top=0;top1=0;
node *x;
if(t==NULL)
	return;


	while(t!=NULL)
	{
		push(t);push1(0);
		t=t->left;
	}
	while(!empty())
	{
		t=pop();
		flag=pop1();
		if(flag!=0)
			cout << t->data;
		else
		{
			push1(1);
			push(t);
			t=t->right;
			while(t!=NULL)
			{
				push(t); push1(0);
				t=t->left;
			}

		}

	}
}

//===================================================
int tree :: val(char p[20])
{

	int i=0,cnt=0;
	while(p[i]!='\0')
	{
		if(isalpha(p[i]))
		{
			cnt++;
			i++;
		}
		else
		{
			cnt--;
			i++;
		}
	}
	if(cnt==1)
	return 1;
	else
	{
		if(cnt==0)
		cout<<"\nInvalid : Operators & Operands are EQUAL";
		cout<<"\n";

		if(cnt<0)
		cout<<"\nInvalid : Operators are MORE than Operands";
		cout<<"\n";

		if(cnt>0)
		cout<<"\nInvalid : Operands are MORE than Operators";
		cout<<"\n";

		return 0;
	}
}

//=======================================================

int main()
{
	tree t;
	t.exp();
}



